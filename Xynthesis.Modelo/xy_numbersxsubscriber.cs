//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Xynthesis.Modelo
{
    using System;
    using System.Collections.Generic;
    
    public partial class xy_numbersxsubscriber
    {
        public string Ide_Number { get; set; }
        public System.DateTime Fec_Date { get; set; }
        public long Ide_Subscriber { get; set; }
        public string Tip_Subscriber { get; set; }
        public int Ide_ServerType { get; set; }
        public int Ide_Branch { get; set; }
        public int Ide_CostCenter { get; set; }
        public int Ide_Geography { get; set; }
        public string Num_AccessCode { get; set; }
        public Nullable<int> Ide_Plan { get; set; }
        public Nullable<int> Vlr_Plan { get; set; }
        public Nullable<int> Num_Minutes { get; set; }
    }
}
