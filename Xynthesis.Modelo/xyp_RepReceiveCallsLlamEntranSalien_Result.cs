//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Xynthesis.Modelo
{
    using System;
    
    public partial class xyp_RepReceiveCallsLlamEntranSalien_Result
    {
        public string Ide_SubscriberEmployee { get; set; }
        public string Nom_Subscriber { get; set; }
        public string calls_unanswering { get; set; }
        public string calls_answering { get; set; }
        public string time_answering { get; set; }
        public string calls_unanswering1 { get; set; }
        public string calls_unanswering2 { get; set; }
        public string calls_unanswering3 { get; set; }
        public string Fec_Date { get; set; }
    }
}
