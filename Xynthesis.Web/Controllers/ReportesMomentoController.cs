﻿using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Xynthesis.AccesoDatos;
using Xynthesis.Modelo;
using Xynthesis.Utilidades;

namespace Xynthesis.Web.Controllers
{
    public class ReportesMomentoController : Controller
    {
        Utilidades.LogXynthesis log = new LogXynthesis();
        Xynthesis.Utilidades.Mensaje msg = new Mensaje();
        xynthesisEntities xyt = new xynthesisEntities();
        public FileStreamResult rep;
        public int num;

        // GET: ReportesMomento
        public ActionResult ReportesMomentaneos()
        {
            if (Session["Ide_Subscriber"] == null)
            {
                return RedirectToAction("Login", "Acceso");
            }
            //ViewData["confProgr"] = xyt.xy_reportes.ToList();
            var lista = xyt.xy_reportes.ToList();
            return View(lista);
        }

        [HttpPost]
        public ActionResult ReportesMomentaneos(string reportes, string formato, string FechaInicial, string FechaFinal)
        {
             return  reporte(Convert.ToInt32(reportes), listaProc(Convert.ToInt32(reportes), FechaInicial, FechaFinal), formato, FechaInicial, FechaFinal, nomrpt(Convert.ToInt32(reportes)));
        }



        public FileStreamResult reporte(int id, Array list , string formato, string FechaInicial, string FechaFinal, string nomrpt)
        {
            ReportDocument rd = new ReportDocument();
            rd.Load(Path.Combine(Server.MapPath("~/Reportes"), nomrpt + ".rpt"));

            rd.SetDataSource(list);

            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();

            try
            {
                if (formato == "pdf")
                {
                    Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/pdf", nomrpt+".pdf");
                }
                else
                {
                    Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/xls", nomrpt+".xls");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Obtener return Lista
        public Array listaProc(int id, string FechaInicial, string FechaFinal)
        {
            if(id == 1) //Reporte ReportePorPeriodoTiempo
            {
                ADReportePorPeriodoTiempo repPorPerioTiempo = new ADReportePorPeriodoTiempo();
                var lista = repPorPerioTiempo.ObtenerListaPorPeriodoTiempo(FechaInicial, FechaFinal, "").ToList();
                return lista.ToArray();
            }
            else if(id == 2) //Reporte ReporteLlamadasEntrantes
            {
                ADReporteLlamadasEntrantes repLlamadaEntrante = new ADReporteLlamadasEntrantes();
                var lista = repLlamadaEntrante.ObtenerListaLlamadasEntrantes(FechaInicial, FechaFinal, "","","").ToList();
                return lista.ToArray();
            }else if (id == 3) //Reporte ReporteLlamadasSalientes
            {
                ADReporteLlamadasSalientes repPorPerioTiempo = new ADReporteLlamadasSalientes();
                var lista = repPorPerioTiempo.ObtenerListaLlamadasSalientes(FechaInicial, FechaFinal, "","").ToList();
                return lista.ToArray();
            }
            else if (id == 4) //Reporte ReporteTiempoDedicado
            {
                ADReporteTiempoDedicado repLlamadaEntrante = new ADReporteTiempoDedicado();
                var lista = repLlamadaEntrante.ObtenerListaTiempoDedicado(FechaInicial, FechaFinal, "").ToList();
                return lista.ToArray();
            }else if (id == 5) //Llamadas ReporteLlamadasEntrantesSalientes
            {
                ADReporteGraficoLlamadasEntrantesSalientes repPorPerioTiempo = new ADReporteGraficoLlamadasEntrantesSalientes();
                var lista = repPorPerioTiempo.ObtenerListaLlamadasEntrantesSalientes(FechaInicial, FechaFinal, "").ToList();
                return lista.ToArray();
            }
            else if (id == 6) //Llamadas ReporteLlamadasRecibidasTransferencias
            {
                ADReporteLlamadasRecibidasTransferidas repLlamadaEntrante = new ADReporteLlamadasRecibidasTransferidas();
                var lista = repLlamadaEntrante.ObtenerListaLlamadasRecibidasTransferidas(FechaInicial, FechaFinal, "").ToList();
                return lista.ToArray();
            }
            else if (id == 7) //ReporteTiempoPromedioAntesContestar
            {
                ADReporteTiempoPromedioAntesContestar repPorPerioTiempo = new ADReporteTiempoPromedioAntesContestar();
                var lista = repPorPerioTiempo.ObtenerListaTiempoPromedioAntesContestar(FechaInicial, FechaFinal, "").ToList();
                return lista.ToArray();
            }
            else if (id == 8) //Reporte FrecuenciaLlamadas
            {
                ADOReporteFrecuenciadellamadas repLlamadaEntrante = new ADOReporteFrecuenciadellamadas();
                var lista = repLlamadaEntrante.ObtenerFrecuenciaDeLlamadas(FechaInicial, FechaFinal).ToList();
                return lista.ToArray();
            }
            else if (id == 9) //Reporte RepPromedioLlamadasHora
            {
                ADRepPromedioLlamadasHora repPorPerioTiempo = new ADRepPromedioLlamadasHora();
                var lista = repPorPerioTiempo.ObtenerPromedioLlamadasHora(FechaInicial, FechaFinal).ToList();
                return lista.ToArray();
            }
            else if (id == 10) //Reporte TopLlamadaCampeonaXCosto
            {
                ADReporteTopLlamadaCampeonaCC repLlamadaEntrante = new ADReporteTopLlamadaCampeonaCC();
                var lista = repLlamadaEntrante.ObtenerTopLlamadaCampeonaCC(FechaInicial, FechaFinal, "", "").ToList();
                return lista.ToArray();
            }
            else if (id == 11) //Reporte LlamadaCampeonaXDuracion
            {
                ADLlamadaCampeonaDuracion repPorPerioTiempo = new ADLlamadaCampeonaDuracion();
                var lista = repPorPerioTiempo.ObtenerLlamadaCampeonaDuracion(FechaInicial, FechaFinal, "","").ToList();
                return lista.ToArray();
            }
            else if (id == 12) //Reporte NumeroMasMarcado
            {
                ADReporteNumeroMasMarcado repLlamadaEntrante = new ADReporteNumeroMasMarcado();
                var lista = repLlamadaEntrante.ObtenerNumeroMasMarcado(FechaInicial, FechaFinal).ToList();
                return lista.ToArray();
            }else if (id == 13) //Reporte FrecuenciaLlamadas
            {
                ADOReporteFrecuenciadellamadas repPorPerioTiempo = new ADOReporteFrecuenciadellamadas();
                var lista = repPorPerioTiempo.ObtenerFrecuenciaDeLlamadas(FechaInicial, FechaFinal).ToList();
                return lista.ToArray();
            }
            else if (id == 14) //Reporte ConsumosPersonales
            {
                ADReporteConsumosPersonales repLlamadaEntrante = new ADReporteConsumosPersonales();
                var lista = repLlamadaEntrante.ObtenerConsumosPersonales("",FechaInicial, FechaFinal, "").ToList(); //un parametro EXTENSION mas +
                return lista.ToArray();
            }
            else if (id == 15) //Reporte ConsumoPorCentrosCostos
            {
                ADReporteConsumosPorCentroCost repLlamadaEntrante = new ADReporteConsumosPorCentroCost();
                var lista = repLlamadaEntrante.ObtenerConsumosPorcentroCostos(FechaInicial, FechaFinal, "").ToList();
                return lista.ToArray();
            }
            else if (id == 16) //Reporte CoberturaLlamadas
            {
                ADReporteCoberturaLlamadas repPorPerioTiempo = new ADReporteCoberturaLlamadas();
                var lista = repPorPerioTiempo.ObtenerCoberturaLlamadas(FechaInicial, FechaFinal, "").ToList();
                return lista.ToArray();
            }
            else if (id == 17) //Reporte HistoriaConsumos
            {
                ADReporteHistoriaConsumo repLlamadaEntrante = new ADReporteHistoriaConsumo();
                var lista = repLlamadaEntrante.ObtenerHistoriaConsumos(FechaInicial, FechaFinal, "", "", "").ToList();
                return lista.ToArray();
            }
            else if (id == 18) //Reporte ReporteGraficoTiempoPromedioAntesDeContestarNuevo
            {
                ADReporteGraficoTiempoPromedioAntesDeContestar repPorPerioTiempo = new ADReporteGraficoTiempoPromedioAntesDeContestar();
                var lista = repPorPerioTiempo.ObtenerListaTiempoPromedioAntesContestar(FechaInicial, FechaFinal).ToList();
                return lista.ToArray();
            }
            else if (id == 19) //Reporte ReporteGraficoEstadisticoLlamadasEntrantesSalientes
            {
                ADReporteGraficoLlamadasEntrantesSalientes repLlamadaEntrante = new ADReporteGraficoLlamadasEntrantesSalientes();
                var lista = repLlamadaEntrante.ObtenerListaLlamadasEntrantesSalientes(FechaInicial, FechaFinal, "").ToList();
                return lista.ToArray();
            }
            else if (id == 20) //Reporte ReporteGraficoLlamadasEntrantesSalientesDuracion
            {
                ADReporteGraficoLlamadasEntrantesSalientesDuracion repPorPerioTiempo = new ADReporteGraficoLlamadasEntrantesSalientesDuracion();
                var lista = repPorPerioTiempo.ObtenerListaLlamadasEntrantesSalientesDuracion(FechaInicial, FechaFinal, "").ToList();
                return lista.ToArray();
            }
            else if (id == 21) //Reporte ReporteConsolidadoCoberturaLLamadas
            {
                ADReporteConsolidadoCoberturaLLamadas repLlamadaEntrante = new ADReporteConsolidadoCoberturaLLamadas();
                var lista = repLlamadaEntrante.ObtenerConsolidadoCoberturaLlamadas(FechaInicial, FechaFinal, "").ToList();
                return lista.ToArray();
            }
            else if (id == 22) //Reporte RepPromedioLlamadasHora
            {
                ADRepPromedioLlamadasHora repPorPerioTiempo = new ADRepPromedioLlamadasHora();
                var lista = repPorPerioTiempo.ObtenerPromedioLlamadasHora(FechaInicial, FechaFinal).ToList(); //====================================
                return lista.ToArray();
            }
            else if (id == 23) //Reporte RepTarificacionEntraSalieTrans
            {
                ADRepTarificacionEntraSalieTrans repLlamadaEntrante = new ADRepTarificacionEntraSalieTrans();
                var lista = repLlamadaEntrante.ObtenerTarificacion(FechaInicial, FechaFinal).ToList();
                return lista.ToArray();
            }
            else //Reporte ReporteLlamadasAbiertasCerradas
            {
                ADReporteLlamadasAbiertasCerradas repLlamadaEntrante = new ADReporteLlamadasAbiertasCerradas();
                var lista = repLlamadaEntrante.ObtenerListaLlamadasAbiertasCerradas(FechaInicial, FechaFinal, "", "", "").ToList();
                return lista.ToArray();
            }
            
        }

        public string nomrpt(int id)
        {
            if(id == 1)
            {
                string nom = "ReportePorPeriodoTiempo";
                return nom;
            }else if(id == 2)
            {
                string nom = "ReporteLlamadasEntrantes";
                return nom;
            }else if (id == 3)
            {
                string nom = "ReporteLlamadasSalientes";
                return nom;
            }
            else if (id == 4)
            {
                string nom = "ReporteTiempoDedicado";
                return nom;
            }
            else if (id == 5)
            {
                string nom = "ReporteLlamadasEntrantesSalientes";
                return nom;
            }
            else if (id == 6)
            {
                string nom = "ReporteLlamadasRecibidasTransferencias";
                return nom;
            }
            else if (id == 7)
            {
                string nom = "ReporteTiempoPromedioAntesContestar";
                return nom;
            }
            else if (id == 8)
            {
                string nom = "FrecuenciaLlamadas";
                return nom;
            }
            else if (id == 9)
            {
                string nom = "RepPromedioLlamadasHora";
                return nom;
            }
            else if (id == 10)
            {
                string nom = "TopLlamadaCampeonaXCosto";
                return nom;
            }
            else if (id == 11)
            {
                string nom = "LlamadaCampeonaXDuracion";
                return nom;
            }
            else if (id == 12)
            {
                string nom = "NumeroMasMarcado";
                return nom;
            }
            else if (id == 13)
            {
                string nom = "FrecuenciaLlamadas";
                return nom;
            }
            else if (id == 14)
            {
                string nom = "ConsumosPersonales";
                return nom;
            }
            else if (id == 15)
            {
                string nom = "ConsumoPorCentrosCostos";
                return nom;
            }
            else if (id == 16)
            {
                string nom = "CoberturaLlamadas";
                return nom;
            }
            else if (id == 17)
            {
                string nom = "HistoriaConsumos";
                return nom;
            }
            else if (id == 18)
            {
                string nom = "ReporteGraficoTiempoPromedioAntesDeContestarNuevo";
                return nom;
            }
            else if (id == 19)
            {
                string nom = "ReporteGraficoEstadisticoLlamadasEntrantesSalientes";
                return nom;
            }
            else if (id == 20)
            {
                string nom = "ReporteGraficoLlamadasEntrantesSalientesDuracion";
                return nom;
            }
            else if (id == 21)
            {
                string nom = "ReporteConsolidadoCoberturaLLamadas";
                return nom;
            }
            else if (id == 22)
            {
                string nom = "RepPromedioLlamadasHora";
                return nom;
            }
            else if (id == 23)
            {
                string nom = "RepTarificacionEntraSalieTrans";
                return nom;
            }
            else
            {
                string nom = "ReporteLlamadasAbiertasCerradas";
                return nom;
            }
            
        }
    }
}